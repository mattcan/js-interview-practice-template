# Javascript (Node) Template For Interview Practice

A quick way to get my preferred tools and folder structure. Maybe you'll like it
too.

## Usage

1. Clone repository into a new folder
	```sh
	git clone https://gitlab.com/mattcan/js-interview-practice-template.git name-of-problem
	```
1. Install packages
	```sh
	cd name-of-problem
	npm install
	```
1. Optionally, change or remove remote URL
	```sh
	# change
	git remote origin set-url https://google.ca

	# remove
	git remote remove origin
	```

You can now get to work without mucking about.

## Tool and Structure Choices

* I like jest
* I like two-space tabs
* I like large test data separate from the tests
* I like NVM and Node LTS

This project is meant to be forked.

